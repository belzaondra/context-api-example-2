import axios from "axios";
import { createContext, useEffect, useState } from "react";
import { v4 } from "uuid";

const reducer = (state, action) => {
  switch (action.name) {
    case "DELETE_USER":
      return [...state.filter((u) => u.id !== action.payload)];
    case "CREATE_USER":
      return [{ ...action.payload, id: v4() }, ...state];
    default:
      throw Error("Invalid action name!");
  }
};

export const usersContext = createContext(null);

export const UsersProvider = ({ children }) => {
  const [users, setUsers] = useState([]);
  const dispatch = (action) => {
    setUsers((u) => reducer(u, action));
  };
  useEffect(() => {
    axios
      .get("https://jsonplaceholder.typicode.com/users")
      .then((resp) => setUsers(resp.data))
      .catch((err) => console.error(err));
  }, []);
  return (
    <usersContext.Provider value={{ users, dispatch }}>
      {children}
    </usersContext.Provider>
  );
};
