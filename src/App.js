import UserList from "./components/userList";
import AddUser from "./components/addUser";
function App() {
  return (
    <div className="App">
      <AddUser />
      <UserList />
    </div>
  );
}

export default App;
