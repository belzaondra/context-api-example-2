import { useContext } from "react";
import { usersContext } from "../providers/usersProvider";
const UserList = () => {
  const { users, dispatch } = useContext(usersContext);
  return (
    <>
      <h1>List of users</h1>
      {/* {JSON.stringify(users)} */}
      {users.map((u, i) => (
        <div key={u.id}>
          <p>Name: {u.name}</p>
          <p>Email: {u.email}</p>
          <button
            onClick={() => dispatch({ name: "DELETE_USER", payload: u.id })}
          >
            delete{" "}
          </button>
          {i !== users.length - 1 && <hr />}
        </div>
      ))}
    </>
  );
};
export default UserList;
