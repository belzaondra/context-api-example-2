import { useContext, useState } from "react";
import { usersContext } from "../providers/usersProvider";

const AddUser = () => {
  const { dispatch } = useContext(usersContext);
  const [user, setUser] = useState({
    name: "John Doe",
    email: "example@example.com",
  });

  const createUser = (e) => {
    e.preventDefault();
    dispatch({ name: "CREATE_USER", payload: user });
    setUser({ name: "", email: "" });
  };

  const formChanged = (e) => {
    setUser({ ...user, [e.target.name]: e.target.value });
  };
  return (
    <>
      <form onSubmit={createUser}>
        <h2>Add new user</h2>
        <p>name: </p>
        <input name="name" value={user.name} onChange={formChanged} />
        <p>email: </p>
        <input name="email" value={user.email} onChange={formChanged} />
        <br />
        <button type="submit">Add user</button>
      </form>
    </>
  );
};
export default AddUser;
